flask-openid (1.3.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.3.0+dfsg (Closes: #997620).
  * d/control: Bump Standards-Version to 4.6.0.1.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Tue, 23 Nov 2021 23:24:25 -0300

flask-openid (1.2.5+dfsg-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-flask-openid-doc: Add Multi-Arch: foreign.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 21 Aug 2021 13:44:36 +0100

flask-openid (1.2.5+dfsg-4) unstable; urgency=medium

  [ Emmanuel Arias ]
  * New maintainer. (Closes: #873651)
  * d/control: Use debhelper-compat instead of debhelper
    - Bump debhelper-compat to 13
  * d/control: Bump standard-version to 4.5.0
  * d/control: Set DPMT as Maintainer and me as Uploaders
  * d/copyright: Add me on copyright file
  " d/rules: fix override_auto_build and override_auto_clean targets
  * d/gbp.conf: Set as branch debian/master as default.
  * d/salsa-ci.yml: Enable salsa-ci.
  * Run wrap-and-sort.

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field.
  * d/control: Set Vcs-* to salsa.debian.org.
  * Enable autopkgtest-pkg-python testsuite.
  * d/rules: Set PYBUILD_NAME and remove python3-flask-openid.install.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Thu, 10 Sep 2020 17:10:02 -0300

flask-openid (1.2.5+dfsg-3) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group.
  * Drop Python 2 support (Closes: #936527).

 -- Ondřej Nový <onovy@debian.org>  Tue, 10 Dec 2019 15:25:47 +0100

flask-openid (1.2.5+dfsg-2) unstable; urgency=medium

  * debian/control: Actually save the file before running dpkg-buildpackage.

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 22 Sep 2015 19:17:34 +0200

flask-openid (1.2.5+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump python3-flask (Build-)Depends to match the version in setup.py.
    - Move to collab-maint.
      + Update Maintainer.
      + Update Vcs-Git.

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 22 Sep 2015 18:59:28 +0200

flask-openid (1.2.4+dfsg-2) unstable; urgency=medium

  * Upload to unstable.

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 25 Apr 2015 12:15:50 +0200

flask-openid (1.2.4+dfsg-1) experimental; urgency=medium

  * New upstream release.

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 29 Oct 2014 02:26:13 +0100

flask-openid (1.2.3+dfsg-3) unstable; urgency=medium

  * Upload to unstable.

 -- Sebastian Ramacher <sramacher@debian.org>  Fri, 24 Oct 2014 15:44:33 +0200

flask-openid (1.2.3+dfsg-2) experimental; urgency=medium

  * Add package for Python 3.
    - debian/control: Add python3-flask, python3-all, python3-openid,
      python3-setuptools and python3-openid to Build-Depends.
    - debian/rules: Build with --with python3.
  * debian/python-flask-openid{,-doc}.examples: Move examples to
    python-flask-openid-doc package.
  * debian/{rules,copyright}: Use uscan's File-Excluded support instead of
    custom get-orig-source-target.

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 23 Oct 2014 14:18:42 +0200

flask-openid (1.2.3+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Bump Standards-Version to 3.9.6, no changes required.

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 23 Oct 2014 01:21:40 +0200

flask-openid (1.2.1+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Use pybuild buildsystem:
    - debian/rules: Add --buildsystem=pybuild to dh call.
    - debian/control: Add dh-python to Build-Depends.
  * debian/control: Bump Standards-Version to 3.9.5, no changes required.
  * debian/copyright: Update copyright years.
  * debian/patches
    - sphinx-default-theme.patch: Removed, no longer needed.
    - github-fork.patch: Remove "Fork from github" button to fix
      privacy-breach-generic from lintian.
  * Use dh_python2 to remove SOURCES.txt:
    - debian/python-flask-openid.pyremove: Add SOURCES.txt.
    - debian/rules: Remove override_dh_auto_install.

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 29 Jan 2014 19:08:59 +0100

flask-openid (1.1.1+dfsg-2) unstable; urgency=low

  * Upload to unstable.
  * debian/clean: Remove Flask_OpenID.egg-info/* to fix FTBFS if built twice
    in a row.

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 04 May 2013 23:06:29 +0200

flask-openid (1.1.1+dfsg-1) experimental; urgency=low

  * Initial release. (Closes: #703534)

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 24 Mar 2013 20:29:21 +0100
